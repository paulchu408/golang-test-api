# 使用官方 Golang 基礎鏡像
FROM golang:1.22

# 設定工作目錄
WORKDIR /app

# 將 go.mod 和 go.sum 文件複製到容器中
COPY go.mod ./
COPY go.sum ./

# 下載依賴包
RUN go mod download

# 將源代碼文件複製到容器中
COPY . .

# 安裝 migrate CLI
RUN go install -tags 'postgres' github.com/golang-migrate/migrate/v4/cmd/migrate@latest

# 編譯你的應用程序
RUN go build -o /docker-golang-app

# 執行應用程序
# CMD [ "/docker-golang-app" ]
