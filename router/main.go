package router

import (
	"github.com/gofiber/fiber/v3"
	"github.com/paulchu/gapi/api"
)

func Set(app *fiber.App) {
	app.Get("/", func(c fiber.Ctx) error {
		return c.SendString("Hello World!")
	})
	app.Get("/user/all", api.GetAllUsers)
	app.Get("/user/:name", api.GetUser)
	app.Post("/user", api.CreateUser)
	app.Post("/product", api.CreateProduct)

}
