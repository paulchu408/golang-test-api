package main

import (
	"github.com/gofiber/fiber/v3"
	"github.com/paulchu/gapi/config"
	"github.com/paulchu/gapi/pkg/postgres"
	"github.com/paulchu/gapi/router"
)

func main() {
	appPort := ":" + config.GetAppPort()

	postgres.Connect("default")

	app := fiber.New()
	router.Set(app)

	app.Listen(appPort)
	// mux := http.NewServeMux()

	// postgres.Connect("default")

	// router.Set(mux)

	// muxHandler := middleware.SetJsonResponse(mux)
	// fmt.Println("Server is running on:", appPort)
	// if err := http.ListenAndServe(":"+appPort, muxHandler); err != nil {
	// 	log.Fatal(err)
	// }
}
