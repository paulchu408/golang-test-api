#!/bin/bash
# 停止腳本執行當遇到錯誤
go mod tidy
set -e
make migrate-up
tail -f /dev/null
