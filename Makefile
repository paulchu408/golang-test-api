include .env
export

migrate-up:
	migrate -path /app/pkg/db/migrations -database "postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${DOCKER_HOST}:${DB_PORT}/${DB_NAME}?sslmode=disable" -verbose up
	migrate -path /app/pkg/db/migrations -database "postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${DOCKER_HOST}:${TEST_DB_PORT}/${TEST_DB_NAME}?sslmode=disable" -verbose up