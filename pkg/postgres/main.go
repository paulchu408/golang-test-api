package postgres

import (
	"fmt"
	"log"

	"github.com/paulchu/gapi/config"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var Database *Psdb

type Psdb struct {
	DB *gorm.DB
}

func Connect(s string) {
	dsn := getConnectionString(s)
	Database = connect(dsn)
}

func (db *Psdb) Close() {
	Database.Close()
}

func getConnectionString(s string) string {
	var dbName string
	config := config.GetPsqlConfig()
	switch s {
	case "test":
		dbName = config.TestDBName
	default:
		dbName = config.DBName
	}
	dsn := fmt.Sprintf(
		"host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Asia/Shanghai",
		config.Host,
		config.UserName,
		config.Password,
		dbName,
		config.Port,
	)
	return dsn
}

func connect(dsn string) *Psdb {
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal("Database connect err:", err)
	}
	fmt.Println("Connect success.")
	return &Psdb{
		DB: db,
	}
}
