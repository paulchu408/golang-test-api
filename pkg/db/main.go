package db

import "runtime"

type CustomErr struct {
	msg string
}

func (e CustomErr) Error() string {
	return e.msg
}

func whosCall() string {
	pc, _, _, _ := runtime.Caller(1)
	details := runtime.FuncForPC(pc)
	if details == nil {
		return "unknown"
	}
	return details.Name()
}
