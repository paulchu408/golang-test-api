package db

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/google/uuid"
	"github.com/paulchu/gapi/pkg/postgres"
)

type Product struct {
	ID       uuid.UUID `gorm:"type:uuid;primary_key;default:gen_random_uuid()"`
	CreateAt time.Time `gorm:"type:timestamp;not null;default:current_timestamp"`
	Name     string    `gorm:"type:text"`
	Price    int       `gorm:"type:int"`
	Stock    int       `gorm:"type:int"`
	IsDelete bool      `gorm:"type:boolean;default:false"`
}

type ProductLog struct {
	UserId   uuid.UUID `gorm:"type:uuid"`
	FuncName string    `gorm:"type:text"`
	OldValue string    `gorm:"type:text"`
	NewValue string    `gorm:"type:text"`
	ErrMsg   string    `gorm:"type:text"`
	CreateAt time.Time `gorm:"type:timestamp"`
}

func CreateProduct(p *Product) (*Product, error) {
	// 這邊應該存入request帶回來的UserId
	log := &logParam{
		id:       uuid.Nil,
		ov:       Product{},
		funcName: whosCall(),
	}

	// 檢查並創建資料表
	postgres.Database.DB.AutoMigrate(p)

	log.nv = p
	r := postgres.Database.DB.Create(p)
	if r.Error != nil {
		log.err = r.Error.Error()
		go log.logger()
		return nil, r.Error
	}

	go log.logger()
	return p, nil
}

func UpdateProductStock(pid uuid.UUID, n int) (*int, error) {
	// 這邊應該存入request帶回來的UserId
	log := &logParam{
		id:       uuid.Nil,
		funcName: whosCall(),
	}
	p := Product{}

	r := postgres.Database.DB.Where(&Product{ID: pid, IsDelete: false}).First(&p)
	if r.Error != nil {
		return nil, CustomErr{msg: "查無商品"}
	}

	log.ov = p // 複製一份給log用
	newStock := p.Stock + n
	if newStock < 0 {
		log.err = "庫存不足"
		go log.logger()
		return nil, CustomErr{msg: fmt.Sprintf("庫存不足，目前僅剩 %v 件", p.Stock)}
	}

	updr := postgres.Database.DB.Model(&p).Update("Stock", newStock)
	if updr.Error != nil {
		log.nv = &p
		log.err = updr.Error.Error()
		go log.logger()
		return nil, CustomErr{msg: "更新失敗"}
	}

	go log.logger()
	return &newStock, nil
}

type logParam struct {
	funcName string
	id       uuid.UUID
	ov       Product
	nv       *Product
	err      string
}

func (p *logParam) logger() {
	obytes, _ := json.Marshal(p.ov)
	nbytes, _ := json.Marshal(p.nv)

	log := &ProductLog{
		FuncName: p.funcName,
		UserId:   p.id,
		OldValue: string(obytes),
		NewValue: string(nbytes),
		ErrMsg:   p.err,
		CreateAt: time.Now(),
	}

	postgres.Database.DB.AutoMigrate(log)
	r := postgres.Database.DB.Create(log)
	if r.Error != nil {
		fmt.Println(r.Error)
	}
}
