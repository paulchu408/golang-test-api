package db

import (
	"time"

	"github.com/google/uuid"
	"github.com/paulchu/gapi/pkg/postgres"
)

type User struct {
	ID       uuid.UUID `gorm:"type:uuid;primary_key;default:gen_random_uuid()"`
	CreateAt time.Time `gorm:"type:timestamp;not null;default:current_timestamp"`
	Name     string    `gorm:"type:text;not null;"`
	Age      int       `gorm:"type:int;not null;"`
	Email    string    `gorm:"type:text;not null;"`
	IsActive bool      `gorm:"type:boolean;default:false;"`
	IsDelete bool      `gorm:"type:boolean;default:false"`
}

func FindFirstUser() (*User, error) {
	user := &User{}
	r := postgres.Database.DB.First(user)
	return user, r.Error
}

func GetAllUser() (*[]User, error) {
	var users []User
	r := postgres.Database.DB.Find(&users)
	return &users, r.Error
}

func FindByName(n string) (*User, error) {
	user := &User{}
	r := postgres.Database.DB.Find(user, "name = ?", n)
	return user, r.Error
}

func CreateUser(u *User) (*User, error) {
	postgres.Database.DB.AutoMigrate(u)
	r := postgres.Database.DB.Create(u)
	return u, r.Error
}
