CREATE TABLE "users" (
  "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
  "create_at" timestamp NOT NULL DEFAULT (now()),
  "name" text,
  "age" int,
  "email" text
);

CREATE TABLE "wallet" (
  "id" bigserial PRIMARY KEY,
  "user_id" uuid NOT NULL,
  "balance" decimal NOT NULL DEFAULT 0,
  "create_at" timestamp NOT NULL DEFAULT (now())
);

ALTER TABLE "wallet" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");