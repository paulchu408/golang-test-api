package handler

import (
	"fmt"
	"net/http"
	"reflect"
)

func RespHandler(fn interface{}, args ...interface{}) func(w http.ResponseWriter, r *http.Request) {
	// 確定傳入的資料為函數
	if reflect.TypeOf(fn).Kind() != reflect.Func {
		panic("傳入必須是函數")
	}
	var reflectArgs []reflect.Value
	for _, arg := range args {
		reflectArgs = append(reflectArgs, reflect.ValueOf(arg))
	}
	return func(w http.ResponseWriter, r *http.Request) {
		result := reflect.ValueOf(fn).Call(reflectArgs)
		fmt.Fprintln(w, result)
	}
}
