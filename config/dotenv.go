package config

import (
	"log"
	"os"

	"github.com/joho/godotenv"
)

func init() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("加載env檔案失敗: %v", err)
	}
}

type PsqlConfig struct {
	Host       string
	Port       string
	UserName   string
	Password   string
	DBName     string
	TestDBName string
}

func GetPsqlConfig() PsqlConfig {
	return PsqlConfig{
		os.Getenv("HOST_NAME"),
		os.Getenv("DB_PORT"),
		os.Getenv("POSTGRES_USER"),
		os.Getenv("POSTGRES_PASSWORD"),
		os.Getenv("DB_NAME"),
		os.Getenv("TEST_DB_NAME"),
	}
}

func GetAppPort() string {
	p := os.Getenv("APP_PORT")
	if p == "" {
		log.Fatal("Port號未定義")
	}
	return p
}
