package api

import (
	"fmt"

	"github.com/gofiber/fiber/v3"
	"github.com/paulchu/gapi/pkg/db"
)

func CreateProduct(c fiber.Ctx) error {
	p := &db.Product{}

	if err := c.Bind().Body(p); err != nil {
		fmt.Println(err)
		return result(fiber.StatusBadRequest, "參數錯誤")
	}

	_, err := db.CreateProduct(p)
	if err != nil {
		fmt.Println(err)
		return result(fiber.StatusBadRequest, "創建失敗")
	}

	return c.JSON(p)
}
