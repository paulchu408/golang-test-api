package api

import (
	"encoding/json"
	"net/http"

	"github.com/gofiber/fiber/v3"
	"github.com/google/uuid"
	"github.com/paulchu/gapi/pkg/db"
)

func FindFirstUser(w http.ResponseWriter, r *http.Request) {
	user, err := db.FindFirstUser()
	if err != nil {
		http.Error(w, "請求失敗", http.StatusBadRequest)
		return
	}
	json.NewEncoder(w).Encode(user)
}

func GetAllUsers(c fiber.Ctx) error {
	users, err := db.GetAllUser()
	if err != nil {
		return result(fiber.StatusBadRequest, "查詢錯誤")
	}
	return c.JSON(users)
}

func GetUser(c fiber.Ctx) error {
	n := c.Params("name")
	if n != "" {
		user, err := db.FindByName(n)
		if err != nil {
			msg := result(fiber.StatusBadRequest, err.Error())
			return msg
		}
		if user.ID == uuid.Nil {
			return result(fiber.StatusBadRequest, "查無使用者")
		}
		return c.JSON(user)
	}
	return result(fiber.StatusBadRequest, "參數錯誤")
}
func CreateUser(c fiber.Ctx) error {
	user := new(db.User)
	if err := c.Bind().Body(user); err != nil {
		return result(fiber.StatusBadRequest, "Post參數錯誤")
	}

	_, err := db.CreateUser(user)
	if err != nil {
		return result(fiber.StatusBadRequest, "創建失敗")
	}
	return c.JSON(user)
}
func PutUser(r *fiber.Ctx) error {
	return nil
}
func DeleteUser(r *fiber.Ctx) error {
	return nil
}
