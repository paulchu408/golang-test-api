package api

// 自訂result格式
type ResMsg struct {
	StatusCode int
	Msg        string
}

func (r ResMsg) Error() string {
	return r.Msg
}

func result(statsuCode int, errMsg string) ResMsg {
	return ResMsg{
		StatusCode: statsuCode,
		Msg:        errMsg,
	}
}
